(in-package :trivial-repl)

(defparameter *running-repl* nil "Indicates that a Trivial REPL is already running.")

(defclass trivial-repl-entry ()
  ((command :initarg :command
	    :reader command
	    :type string)
   (shortcuts :initarg :shortcuts
	     :reader shortcuts
	     :type (or character cons))
   (description :initarg :description
		:reader description
		:type string)
   (action :initarg :action
	   :reader action
	   :type function)))

(defclass trivial-repl ()
  ((name :initarg :name
	 :initform (error "Every Trivial REPL needs to have a name.")
	 :reader repl-name)
   (title :initarg :title
	  :accessor title
	  :initform ""
	  :type string)
   (entries :initarg :entries
	    :accessor entries
	    :type list)
   (should-exit :initform nil
		:accessor should-exit
		:type boolean)
   (welcome-msg :initarg :welcome-msg
		:initform ""
		:reader welcome-msg
		:type string)
   (goodbye-msg :initarg :goodbye-msg
		:initform ""
		:reader goodbye-msg
		:type string)
   (pre-help-msg :initarg :pre-help-msg
		 :initform ""
		 :reader pre-help-msg
		 :type string)
   (post-help-msg :initarg :post-help-msg
		  :initform ""
		  :reader post-help-msg
		  :type string)
   (show-named-prompt :initarg :show-named-prompt
		      :initform nil
		      :type boolean
		      :reader repl-show-named-prompt)
   (prompt :initarg :prompt
	   :type (or character string)
	   :reader repl-prompt)))

(defun create-repl-entry (command shortcuts description action)
  (make-instance 'trivial-repl-entry
		  :command command
		  :shortcuts shortcuts
		  :description description
		  :action action))
		  
(defun highlight-shortcut (command shortcuts)
  "Creates a new string similar to *command* but replacing the first occurence of any *shortcuts* by the first supplied shortcut enclosed by brackets."
  (let ((found nil))
  (with-output-to-string (stream)
    (iterate:iterate
      (iterate:for c in-string (string-downcase command))
      (if (and (not found)
	       (member c shortcuts :test #'char=))
	  (progn
	    (setf found t)
	    (format stream "[~A]" (car shortcuts)))
	  (format stream "~a" c)))
    (unless found
      (format stream " [~A]" (car shortcuts))))))
  
(defun find-matching-entry (command entries)
  (declare (type string command))
  (let ((found nil))
    (iterate:iterate
      (iterate:until found)
      (iterate:for entry in entries)
      (when (or (and (eq 1 (length command))
		     (member (char command 0) (shortcuts entry) :test #'char=))
		(string-equal command (command entry)))
	(setf found entry)))

    (return-from find-matching-entry found)))

(defun extract-command (line)
  (car (split-sequence:split-sequence #\SPACE line)))

(defun extract-args (line)
  (list (cdr (split-sequence:split-sequence #\SPACE line))))

(defun add-standard-repl-entries (repl)
  (unless (find-matching-entry "H" (entries repl))
    (push (make-instance 'trivial-repl-entry
		  :command  "Help"
		  :shortcuts '(#\H #\h #\?)
		  :description "Show this help"
		  :action #'show-repl-help)
	  (slot-value repl 'entries)))

  (unless (find-matching-entry "Q" (entries repl))
    (nconc (slot-value repl 'entries)
		  (list
		   (if *running-repl*
		       (make-instance 'trivial-repl-entry
				      :command   "Return"
				      :shortcuts '(#\Q #\q)
				      :description (format nil "Return to ~a"
							   (repl-name (car *running-repl*)))
				      :action  #'quit-repl)
		       (make-instance 'trivial-repl-entry
				      :command   "Quit"
				      :shortcuts '(#\Q #\q)
				      :description "Exit the REPL"
				      :action  #'quit-repl))))))

(defun show-repl-help (repl args)
  (declare (type trivial-repl repl)
	   (ignore args))
  (trivial-utilities:aif (pre-help-msg repl)
			 (format t "~%~a~%" it)
			 (format t "~%"))

  (iterate:iterate
    (iterate:for entry in (entries repl))
    (format t "~3t~a - ~a~%" (highlight-shortcut (command entry) (shortcuts entry)) (description entry)))

  (trivial-utilities:awhen (post-help-msg repl)
    (format t "~%~a~%" it)))

(defun quit-repl (repl args)
  (declare (type trivial-repl repl)
	   (ignore args))
  (setf (should-exit repl) t))

(defun check-shortcut-conflicts (repl)
  nil)


(defun create-repl (&key name (title "") (prompt ">") (show-named-prompt nil) (entries nil) (welcome-msg "") (goodbye-msg "") (pre-help-msg "") (post-help-msg ""))
  "Create a new instance of trivial-repl to be fed to *#'run-repl*.  
**:NAME** -  
**:TITLE** -  
**:PROMPT** -  
**:SHOW-NAMED-PROMPT** -  
**:ENTRIES** -  
**:WELCOME-MSG** -  
**:GOODBYE-MSG** -  
**:PRE-HELP-MSG** -  
**:POST-HELP-MSG** -"
  (make-instance 'trivial-repl
		  :name (or name (error "Every Trivial REPL needs to have a name."))
		  :title title
		  :prompt prompt
		  :show-named-prompt show-named-prompt
		  :entries (iterate:iterate
			     (iterate:for (command shortcuts description action) in entries)
			     (iterate:collect (create-repl-entry command shortcuts description action)))
		  :welcome-msg welcome-msg
		  :goodbye-msg goodbye-msg
		  :pre-help-msg pre-help-msg
		  :post-help-msg post-help-msg))

(defun run-repl (repl)
  "Enter the *REPL*."
  (declare (type trivial-repl repl))

  (add-standard-repl-entries repl)
  (push repl *running-repl*)

  (unwind-protect
       (progn
	 (setf (should-exit repl) nil)
	 (format t "~%~a~%" (welcome-msg repl))
	 (iterate:iterate
	   (iterate:until (should-exit repl))
	   (format t "~%~{~a~^/~}~a " (when (repl-show-named-prompt (car (last *running-repl*))) (reverse (mapcar #'repl-name *running-repl*))) (repl-prompt repl))
	   (let* ((command (read-line))
		  (entry (find-matching-entry (extract-command command) (entries repl))))
	     (if entry
		 (apply (action entry) repl (extract-args command))
		 (show-repl-help repl nil))))
	 (format t "~%~a~%" (goodbye-msg repl)))
    (setf *running-repl* (cdr *running-repl*))))



