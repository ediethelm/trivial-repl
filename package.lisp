(uiop:define-package #:trivial-repl
  (:documentation "")
  (:use #:common-lisp)
  (:export #:create-repl
	   #:trivial-repl
	   #:run-repl))

