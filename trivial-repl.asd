;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :trivial-repl
  :name "trivial-repl"
  :description ""
  :version "0.0.5"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-utilities
	       :log4cl
	       :iterate)
  :in-order-to ((test-op (test-op :trivial-repl/test)))
  :components ((:file "package")
	       (:file "trivial-repl")))

(defsystem :trivial-repl/test
  :name "trivial-repl/test"
  :description "Unit Tests for the trivial-repl project."
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-repli fiveam)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :trivial-repl-tests))
  :components ((:file "test-trivial-repl")))

